# HPDXtool  

HPDXtool ist wie der Name sagt ein Tool zum aufbereiten von HPGL-, DXF R12- und Profil-Dateien für CNC. Es kann CNC-G-Code für Fräsen und speziell auch für 4-Achs Styroschneiden erstellt werden.  
####  HPDXtool arbeitet intern mit geraden Linien!  
### Importformate:  
- HPGL, Plt wird z.B. immer noch von Inkscape unterstützt.  
- DXF (R12) Zeichenelemente.  
- DAT, Pro Profildateien. Es werden gerade Linien zwischen den Punkten erstellt.  

### Exportformate:  
- HPGL, Plt  
- DXF (Auszug)  
- DAT  
- G-Code, im XYZ-Format für 3-Achs-Maschinen.  
- G-Code, im XYUV-Format für Styroschneiden.  
### Features:  
HPDXtool bietet viele Möglichkeiten die Linienelemente für Fräs- und Schneidmaschinen aufzuarbeiten.  
<b>Reihenfolge:</b> HPDXtool bietet unter anderem die wichtige Möglichkeit die Fräsreihenfolge manuell festzulegen. <br>
<b>Umdrehen:</b> Die Richtung wird umgedreht.<br>
<b>Kette:</b> Alle Linien mit gleichem Anfangs- oder Endpunkt werden zusammengefügt (Polylinie).<br>
<b>Zusammenfügen:</b> Zwei Linien werden verbunden.<br>
<b>Startpunkt:</b> Bei geschlossenen Linienzügen kann der Anfangspunkt verschoben werden.<br>
<b>Trennen:</b> Eine Linienkette kann an einem Zwischenpunkt aufgetrennt werden.<br>
<b>Punktezahl:</b> Die Punktezahl kann verändert werden.<br>
<b>Versatz:</b> Erstellt eine Bahnkorrektur für CNC-Ausgabe. Diese wird beim Exportieren nicht berücksichtigt.<br>
<b>Schließen:</b> Schließt die NC-Bahn, wenn Anfang- und Endpunkt gleich ist.<br>
<b>Bahnversatz:</b> Ertellt die versetzte Schneidbahn.<br>
<b>CNC:</b> Ertellt eine 2,5D-CNC-Code für Fräsen.<br>
<b>XY-UV:</b> 2x2-Achs-G-Code für Styrochneiden.<br>
<b>Übernehmen:</b> Beendet die Mehrfachauswahl einiger Befehle.<br>
<b>Abbruch:</b> Beendet die Auswahl.  

Nach dem Export als G-Code (XYZ) steht das File der Maschinensteuersoftware wie GRBL, LinuxCNC, Mach3 und Anderen zur Verwendung.  

Beim Importieren von Profilen kann die Größe, Drehung (Schränkung) und Position angegeben werden. Hier empfielt es sich diese mit einem Programm wie Xflr aufzubereiten. Vor allem, wenn diese wenig Stützpunkte haben, ist damit eine Verbesserung zu erreichen.  

Für Styroschneiden ist es notwendig beide Seiten zu synchronisieren. Wenn es vom Cad nicht schon so ausgegeben wird, kann mit Teilen beidseitig-paarweise in Teilbereiche aufgeteilt werden. Danach müssen deren Punkteanzahlen angeglichen werden. Bei der Übernahme nach XY-UV werden die Teilbereiche nacheinander ausgewählt. Zuerst für XY und dann für UV. Damit können sie dann miteinander geschnitten werden.  So können sogar zwei unterschiedliche Konturen synchronisiert werden.  

Beim Export als G-Code (XYUV) müssen für jede Seite die zuvor synchrnisierten Linien ausgewählt werden.  

#### Beachte: Die Punktezahl in HPDXtool ist immer um eins höher als die Linienzahl!  

Eine Linie hat zwei Punkte, den Anfangs- und den Endpunkt. Eine Polylinie hat einen Punkt mehr als Liniensegmente. So hat z.B. ein Rechteck 5 Punkte. Der Anfangs und Endpunkt decken sich!  

### Beispiel Bahnversatz:  
![hpdxtool](Images/hpdxtool_klein.png "hpdxtool")  
Die Linien können je nach Bedarf mit den Funktionen sinnvoll aufbereitet werden. Mit "Bahnversatz" wird der Fräserradius generiert. "CNC" erstellt dann den NC-Code für die Fräse.  

### Beispiel Synchronision für XY-UV:  
Die Synchronisation eines Kreises mit einem Rechteck für eine Heißdrahtschneide als Beispiel, erfolgt in zwei Schritten:  
1. Schritt  
Der Kreis und das Rechteck wird hier mit "T" in je vier Bereiche (Polylinien) aufgeteilt. Diese Bereiche werden so gewählt, daß jeweils zwei zueinander passen.  
2. Schritt  
Die zusammengehörenden Polylinien werden mit "P" mit gleich vielen Zwischenpunkten versehen. Die Polylinie sollte möglichst gerade sein oder schon viele Punkte haben. So wird am wenigsten verfälscht.  

So sieht das Ergebnis in Kutty aus:  
![hpdxtool xyuv](Images/hpdxtool_klein_xyuv_nc.png "hpdxtool xyuv")  
Hinweis: Der berechnete Portalweg ist beim Rechteck ist durch die Kompensierung des Abstandes zum Block nicht gerade. Am Werkstück stimmt es.  
  
  ## Download HPDXtool.exe  
  [HPDXtool Setup.exe](Setup.exe)

